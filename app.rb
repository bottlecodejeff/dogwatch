require "httparty"

BASE_URL = "https://ws.petango.com/webservices/adoptablesearch/wsAdoptableAnimalDetails.aspx"
SLACK_WEBHOOK_URL = "https://hooks.slack.com/services/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"

DOGS = {
  "Bailey" => 44390548,
  "Bandit" => 44390379,
  "Funfetti" => 44416776,
  "Gelato" => 44416769,
  "Ilyssa" => 44323409,
  "Praline" => 44416778,
  "Qdoba" => 44323489,
  "Questa" => 44323487,
  "Quilla" => 44323481,
  "Quintessa" => 44323491,
  "Souffle" => 44416780,
  "Willow" => 44367623,
  # "Elva" => 44388257,
  "Timmy" => 44160198,
  # "Charming" => 44242742, # available
}

AvailableDog = Struct.new(:name, :url)

def build_url(id)
  "#{BASE_URL}?id=#{id}"
end

def check
  available_dogs = []

  DOGS.map do |name, id|
    url = build_url(id)
    resp = HTTParty.get(url)

    if resp.body.match?(/Available/)
      available_dogs << AvailableDog.new(name, url)
    end
  end

  available_dogs
end

available_dogs = check

message =
  if available_dogs.any?
    available_dogs.map do |dog|
      "<#{dog.url}|#{dog.name}> is available!"
    end.join("\n")
  else
    "No dogs on watch are currently available."
  end

HTTParty.post(SLACK_WEBHOOK_URL, {
  body: { text: message }.to_json,
})
